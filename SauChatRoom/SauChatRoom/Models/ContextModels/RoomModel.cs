﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace SauChatRoom.Models.ContextModels
{
    public class RoomModel
    {
        [Required]
        [Key]
        public int RoomID { get; set; }
        [Display(Name = "Fotograf")]
        public string photo { get; set; }
        [Required]
        [Display(Name = "OdaAd")]
        public string RoomName { get; set; }
        [Display(Name = "OlusmaTarih")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "DuzenlemeTarih")]
        public DateTime modifiedDate { get; set; }
    }
}