﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace SauChatRoom.Models.ContextModels
{
    public class ChatRoomContext : DbContext
    {
        public ChatRoomContext() : base(WebConfigurationManager.ConnectionStrings["dbChatRoomString"].ConnectionString)
        {
            

        }
        public DbSet<RoomModel> dbRoom { get; set; }

    }
}