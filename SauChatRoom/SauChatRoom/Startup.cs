﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SauChatRoom.Startup))]
namespace SauChatRoom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
