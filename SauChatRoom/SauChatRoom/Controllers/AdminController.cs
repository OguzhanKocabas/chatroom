﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SauChatRoom.Models.ContextModels;

namespace SauChatRoom.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [Authorize(Roles ="Administrator")]
        public ActionResult IndexPanel()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult RolKontrolIndex()
        {
            return View();
        }
        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult CreateOdaKontrolIndex(ChatRoomContext asd)
        //{
        //    asd.dbRoom.Where(x=>x.RoomID)
        //    return View();
        //}

        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult UpdateOdaKontrolIndex(ChatRoomContext asd)
        //{
        //    asd.dbRoom.Where(x => x.RoomID)
        //    return View();
        //}

        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult DeleteOdaKontrolIndex(ChatRoomContext asd)
        //{
        //    asd.dbRoom.Where(x => x.RoomID)
        //    return View();
        //}

        //// GET: Admin/Details/5
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        //// GET: Admin/Create
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Admin/Create

        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Admin/Edit/5
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //// POST: Admin/Edit/5
        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //// GET: Admin/Delete/5
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: Admin/Delete/5
        //[HttpPost]
        //[Authorize(Roles = "Administrator")]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
