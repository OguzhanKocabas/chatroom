﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SauChatRoom.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(Roles = "User")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Administrator")]
        public ActionResult IndexAdmin()
        {
            return View();
        }

    }
}